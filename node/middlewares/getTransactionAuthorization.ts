import axios from "axios"
//import { json } from 'co-body'


export async function getTransactionAuthorization(ctx: Context, next: () => Promise<any>){
	//const body = await json(ctx.req)

	const {
		vtex: {
			route: {
				params: { transactionId },
			},
		},
	} = ctx

	const http = axios.create({
	    headers: {
	      VtexIdclientAutCookie: ctx.vtex.authToken,
	      "Cache-Control"   : "no-cache",
	      "X-Vtex-Use-Https": true,
	      "Authorization": "Bearer 33e4528f253db5d911a690856f0b77cfa98103c8231bffff869f179125d17d28e52bfadb9f205",
	    }
	});

	try{
		const url      = `https://servaxo1.vicom.mx/tafqaGC/API/v1/system/backoffice/getTid?store=${ctx.vtex.account}&tid=${transactionId}`
		const { data } = await http.get(url);

		const today         = new Date();
		const resposeStatus = data.operationId;
		const balance       = data.description.balance;
		let response        = {};

		if(resposeStatus == "success"){
			console.log(" == Get Authorizations OK == ")
			ctx.status = 200;
			response = [{
				oid : `${transactionId}`,
				value: balance,
				date : `${today.toISOString()}`
			}];
		}else{
			console.log(" == Get Authorizations FAIL == ")
			ctx.status = 401;
		}

		ctx.body = response;
		ctx.set('Cache-Control', 'no-cache,no-store');

		/*console.log(" == Get Authorizations == ")
		console.log(resposeStatus)
		console.log(today)
		console.log(response)
		console.log(data)*/


		/*if(resposeStatus == "success"){
			ctx.status = 200;
			response = [{
				oid : `${transactionId}`,
				value: 0,
				//value: balance,
				date : `${today.toISOString()}`
			}];
		}else{
			ctx.status = 401;
		}

		*/
	//console.log(ctx);
	}catch (error){
		console.log(error)
    }

	
	/*console.log(giftcardId)
	console.log(transactionId)
	console.log(ctx)
	console.log(body)*/

	await next()	
}