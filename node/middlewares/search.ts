import { json } from 'co-body'
import axios from "axios"

//Busca productos giftcards dentro de la orden (No se genera la compra de giftcards con el uso de BIN's de giftcards)
function searchGiftcardInOrder(cartItems: any){
  const arrProductsgiftcards = ['4733','4734','4735']; //Id productos GIFTCARDS (Valdiación)
  let flag                    = false;

  for(var i in cartItems){
    let cartProductId = cartItems[i].productId;
    if(arrProductsgiftcards.includes(cartProductId)){
      flag = true;
    }//end if
  }//end for

  return flag;
}//end function



export async function search(ctx: Context, next: () => Promise<any>) {
  const body = await json(ctx.req)

  const { 
      cart: {redemptionCode},
      cart: {items},
      client: { email },
    } = body;

  let response  = {};
  if(redemptionCode !== null){
    //Busca giftcards dentro de la orden (Rechaza el uso de códigos de giftcards)
    const returnSearch =  searchGiftcardInOrder(items);

    if(!returnSearch){
      //Envía email para validar el número de intentos
      //const return_tries = await sendTriesInfo(email,ctx.vtex.authToken,redemptionCode,ctx.vtex.account);
      try{
        const http = axios.create({
          headers: {
            VtexIdclientAutCookie: ctx.vtex.authToken,
            'Content-Type': 'application/json',
            "Cache-Control"   : "no-cache",
            "X-Vtex-Use-Https": true,
            "Authorization": "Bearer 33e4528f253db5d911a690856f0b77cfa98103c8231bffff869f179125d17d28e52bfadb9f205"
          }
        });

        //Envia intentos de rendeción por usuario
        const urlTries = `https://servaxo1.vicom.mx/tafqaGC/API/v1/system/backoffice/getBalance?bin=${redemptionCode}&store=${ctx.vtex.account}&conexion=${email}`;
        const { data }  = await http.get(urlTries);

        const resposeStatus = data.operationId;
        const detailStatus  = data.description;
        //El Bin es correcto y el usuario no esta bloqueado
        if(resposeStatus == "success"){
          //Token giftcard
          const tokenId = detailStatus.id;
          //El token no se genero correctamente (Regresa error para no procesar el  código)
          if(tokenId === undefined || tokenId == ""){
            ctx.status = 401
          }else{//La busqueda de bin fue existosa
            ctx.status = 200
          }//end if

          response = [
          {
            id          : `${tokenId}`,
            provider    : `axo_giftcard`,
            balance     : detailStatus.balance,
            totalBalance: detailStatus.balance,
            groupName   : `AxoGiftcard`,
            redemptionCode: `${redemptionCode}`,
            _self: {
              href: `${ctx.vtex.account}/giftcardproviders/axo_giftcard`,
            },
          },
          ];

          console.log(" === Search  OK  ===");
          console.log(response);

        }else if(resposeStatus == "error"){
          //No existe Bin o usuario bloqueado
          console.log(" === Search  Fail  ===");
          console.log(email);
          console.log(detailStatus);

          if(detailStatus == "BIN no existente"){
            ctx.status = 401;
          }else{
            ctx.status = 403;
          }//end if

        }//end if

        ctx.body   = response;
        ctx.set('Cache-Control', 'no-cache,no-store');
      }catch (error){
        console.log(error)
      }//end try

    }else{
      console.log (" == Return Search ==");
      console.log(returnSearch);

      ctx.status = 401;
      ctx.set('Cache-Control', 'no-cache,no-store')
    }//end if
  }else{
    ctx.status = 200
    ctx.body   = response;
    ctx.set('Cache-Control', 'no-cache,no-store');
  }//end if

  await next()
}
