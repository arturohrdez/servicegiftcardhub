import { json } from 'co-body'

export async function createSettlement(ctx: Context, next: () => Promise<any>){
	const body = await json(ctx.req)

	const {
		vtex: {
			route: {
				params: { giftcardId, transactionId },
			},
		},
	} = ctx

	const today  = new Date();
	const value  = body.value
	//const data   = body.requestId
	let response = {}

	console.log(" == Settlement Cencellations == ")
	console.log(giftcardId)
	console.log(transactionId)
	console.log(body)

	ctx.status = 200;
	response = {
		oid : `${transactionId}`,
		value: value,
		date : `${today.toISOString()}`
	}

	console.log(response)
	ctx.body   = response;
	ctx.set('Cache-Control', 'no-cache,no-store')
	
	await next()
}