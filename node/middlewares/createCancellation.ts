import { json } from 'co-body'
import axios from "axios"

export async function createCancellation(ctx: Context, next: () => Promise<any>){
	const body = await json(ctx.req)
	
	const {
		vtex: {
			route: {
				params: { giftcardId,transactionId}
			},
		},
	} = ctx

	//console.log(ctx)
	/*console.log("value => " + body.value)
	console.log("requestId => " + body.requestId)
	//console.log(body)
	console.log("giftcardId => " + giftcardId)
	console.log("TID => " + transactionId)
	return false;*/

	const http = axios.create({
	    headers: {
	      VtexIdclientAutCookie: ctx.vtex.authToken,
	      "Cache-Control"   : "no-cache",
	      "X-Vtex-Use-Https": true,
	      "Authorization": "Bearer 33e4528f253db5d911a690856f0b77cfa98103c8231bffff869f179125d17d28e52bfadb9f205",
	    }
	});

	const monto    = body.value;
	const url      = `https://servaxo1.vicom.mx/tafqaGC/API/v1/system/backoffice/submitCancel?store=${ctx.vtex.account}&giftcardId=${giftcardId}&AuthorizationNumber=${transactionId}&monto=${monto}`
	const { data } = await http.get(url);

	/*console.log(" === Monto ====");
	console.log(monto);
	console.log(url);*/

	//response body
	let response        = {};
	const resposeStatus = data.operationId;
	const operationId   = data.description.id;
	const today         = new Date();

	if(resposeStatus == "success"){
		ctx.status = 200;
		response = {
			oid      : `${operationId}`,
			value    : monto,
			date     : `${today.toISOString()}`,
		};
	}else if(resposeStatus == "error"){
		console.log(data.description);
		ctx.status = 401;
	}//end if

	console.log(" == Cancellation ==");
	console.log("requestId => " + body.requestId)
	//console.log(body);
	//console.log(ctx);
	//console.log(url);
	//console.log(body);
	//console.log(giftcardId);
	//console.log(transactionId);
	//return false;
	//console.log(data);

	

  	console.log(response);

	//ctx.status = 200;
	ctx.body   = response;
	ctx.set('Cache-Control', 'no-cache,no-store')
  	ctx.set('Content-Type','application/json');

  	//console.log(ctx);
	await next()
}