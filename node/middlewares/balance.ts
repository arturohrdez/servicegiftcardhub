//import { json } from 'co-body'
import axios from "axios";
//import * as proxyAddr from "proxy-addr";


export async function balance(ctx: Context, next: () => Promise<any>) {
  //const body = await json(ctx.req)

  const {
    vtex: {
      route: { params }
    }
  } = ctx

  const { id } = params

/*  const clientIp = ctx.request.ip;
  const clientHref = ctx.request.href;
  const clientHost = ctx.request.host;
  console.log(ctx.request.headers);
  console.log("Host => " + clientHost);
  console.log("Href => " + clientHref);
  console.log("IP => " + clientIp);*/
  //var trust = proxyAddr.compile('loopback')
  //var addr = proxyAddr.proxyAddr( ctx.request, trust)

  /*console.log("==== Body Balance ====");
  console.log(ctx.state.emailClient)
  console.log("==== Body Balance ====");*/
  

  const http = axios.create({
    headers: {
      VtexIdclientAutCookie: ctx.vtex.authToken,
      'Content-Type': 'application/json',
      "Cache-Control"   : "no-cache",
      "X-Vtex-Use-Https": true,
      "Authorization": "Bearer 33e4528f253db5d911a690856f0b77cfa98103c8231bffff869f179125d17d28e52bfadb9f205",
    }
  });


  //const url = `http://${ctx.vtex.account}.myvtex.com/api/dataentities/awesome_loyalty/documents/${id}?_fields=_all`;
  try {
    //const url = `https://servaxo1.vicom.mx/tafqaGC/API/v1/system/backoffice/getBalance?bin=${id}&store=${ctx.vtex.account}`;
    const url = `https://servaxo1.vicom.mx/tafqaGC/API/v1/system/backoffice/getBalance?token=${id}&store=${ctx.vtex.account}`;
    const { data } = await http.get(url)

    const today = new Date()
    const nextWeek = new Date(today)
    nextWeek.setDate(nextWeek.getDate() + 7)

    const resposeStatus = data.operationId;
    let response = {};

    if(resposeStatus == "success"){
      response = {
        id: data.description.id,
        balance: data.description.balance,
        totalBalance: data.description.balance,
        //emissionDate: `${today.toISOString()}`,
        //expiringDate: `${nextWeek.toISOString()}`,
        provider: `axo_giftcard`,
        discount: false,
        redemptionCode: `${data.description.bin}`,

        transaction: {
          href: `${ctx.vtex.account}/giftcardproviders/axo_giftcard`,
        }
      };

      console.log('== BALANCE OK ==');
      console.log(response);
      
      ctx.status = 200
      ctx.body = response
      ctx.set('Cache-Control', 'no-cache,no-store')
    }else{
      console.log('== BALANCE FAIL ==');
      console.log(resposeStatus)
      console.log(data.description + ctx.vtex.account)
      ctx.status = 401
      ctx.body = response
      ctx.set('Cache-Control', 'no-cache,no-store')
    }//end if
    
  } catch (error) {
    console.log("Error => " + error);
  }

  await next()
}
