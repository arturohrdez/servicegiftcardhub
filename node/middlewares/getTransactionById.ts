//import { json } from 'co-body'
import axios from "axios"

export async function getTransactionById(ctx: Context, next: () => Promise<any>){
	//const body = await json(ctx.req)

	const {
		vtex: {
			route: {
				params: { giftcardId, transactionId },
			},
		},
	} = ctx

	const http = axios.create({
	    headers: {
	      VtexIdclientAutCookie: ctx.vtex.authToken,
	      "Cache-Control"   : "no-cache",
	      "X-Vtex-Use-Https": true,
	      "Authorization": "Bearer 33e4528f253db5d911a690856f0b77cfa98103c8231bffff869f179125d17d28e52bfadb9f205",
	    }
	});

	try{
		const url      = `https://servaxo1.vicom.mx/tafqaGC/API/v1/system/backoffice/getTid?store=${ctx.vtex.account}&tid=${transactionId}`
		const { data } = await http.get(url);

		const today         = new Date();
		const resposeStatus = data.operationId;
		let response = {};

		if(resposeStatus == "success"){
			console.log(" == TransactionById OK == ")
			console.log(data);
			const balance       = data.description.balance;

			ctx.status = 200;
			response = {
				"value"      : balance,
				"description": "",
				"date"       : `${today.toISOString()}`,
				"requestId"  : data.description.autorization,
				"settlement":{
					"href": `${ctx.vtex.account}/_v/axo_giftcard/giftcards/${giftcardId}/transactions/${transactionId}/settlements`
				},
				"cancellation":{
					"href": `${ctx.vtex.account}/_v/axo_giftcard/giftcards/${giftcardId}/transactions/${transactionId}/cancellations`
				},
				"authorization":{
					"href": `${ctx.vtex.account}/_v/axo_giftcard/giftcards/${giftcardId}/transactions/${transactionId}/authorization `	
				},
				"operation":"Debit"
			}
		}else{
			console.log(" == TransactionById FAIL == ")
			console.log(resposeStatus)
			console.log(data.description)
			ctx.status = 401;
		}//end if

		ctx.body = response
		ctx.set('Cache-Control', 'no-cache,no-store')

	}catch(error){
		console.log("Error => " + error);
		console.log("GCID => "+ giftcardId)
		console.log("TID => " + transactionId);
	}//end try

	await next()	
}