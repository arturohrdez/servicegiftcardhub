import axios from "axios"

export async function transactions_update(ctx: Context, next: () => Promise<any>) {
  const { 
    vtex: {
      route: { params }
    }
  } = ctx

  const { id } = params

  //const balance = ctx.state.transactionData.balance - ctx.state.transactionData.transaction.value
  //const balance = ctx.state.transactionData.balance;
  const balance = ctx.state.transactionData.transaction.value;
 //const balance = 1000;

  console.log(balance);
  
  
  const http = axios.create({
    headers: {
      VtexIdclientAutCookie: ctx.vtex.authToken,
      "Cache-Control"   : "no-cache",
      "X-Vtex-Use-Https": true,
      "Authorization": "Bearer 33e4528f253db5d911a690856f0b77cfa98103c8231bffff869f179125d17d28e52bfadb9f205",
    }
  });

  //`http://${ctx.vtex.account}.myvtex.com/api/dataentities/awesome_loyalty/documents/${id}`,
  const url = `https://servaxo1.vicom.mx/tafqaGC/API/v1/system/backoffice/submitPayment?store=${ctx.vtex.account}&bin=${id}&balance=${balance}`;
  const { data } = await http.patch(url);

  const resposeStatus       = data.operationId;
  const AuthorizationNumber = data.description.AuthorizationNumber;
  let response = {};


  if(resposeStatus == "error" || AuthorizationNumber === undefined){
    ctx.status = 401;
  }else if(resposeStatus == "success"){

    if(AuthorizationNumber === undefined){
      ctx.status = 401;
    }else{
      ctx.status = 200;
      response = {
        cardId: id,
        id: AuthorizationNumber,
        provider: `axo_giftcard`,
        _self: {
          href: `${ctx.vtex.account}/giftcardproviders/axo_giftcard`,
         },
      };
    }//end if
  }//end if


  console.log('== TRANSACTION UPDATE ==');  
  console.log(data)
  console.log(response)
  console.log(ctx.state.transactionData)
  console.log("Instancia => " + ctx.vtex.account);
  console.log("orderId =>" + ctx.state.transactionData.transaction.orderInfo.orderId);
  console.log("sequence =>" + ctx.state.transactionData.transaction.orderInfo.sequence);

  //ctx.status = 401
  ctx.body = response
  ctx.set('Cache-Control', 'no-cache,no-store')
  ctx.set('Content-Type','application/json')

  await next()
}
