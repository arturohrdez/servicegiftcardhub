//import { json } from 'co-body'
import axios from "axios"

export async function listTransactions(ctx: Context, next: () => Promise<any>) {
	//const body = await json(ctx.req)
	const {
		vtex : {
			route: { params }
		}
	}= ctx

	const { id } = params

	const http = axios.create({
		headers              : {
			VtexIdclientAutCookie: ctx.vtex.authToken,
			"Cache-Control"      : "no-cache",
			"X-Vtex-Use-Https"   : true,
			"Authorization"      : "Bearer 33e4528f253db5d911a690856f0b77cfa98103c8231bffff869f179125d17d28e52bfadb9f205",
		}
	});

	

	try{
		const url      = `https://servaxo1.vicom.mx/tafqaGC/API/v1/system/backoffice/getBalance?token=${id}&store=${ctx.vtex.account}`;
		const { data } = await http.get(url)

		const resposeStatus = data.operationId;
		let response = {};

		if(resposeStatus == "success"){
			console.log(" == List Transactions OK ==");
			response      = [{
				cardId: data.description.id,
				id    : data.description.bin,
				_self: {
					href: `${ctx.vtex.account}/giftcardproviders/axo_giftcard`,
				},
			}];
			ctx.status = 200
		}else{
			console.log(" == List Transactions FAIL ==");
			console.log(resposeStatus)
			console.log(data.description)
			ctx.status = 401
		}//end if

		ctx.body = response
		ctx.set('Cache-Control', 'no-cache,no-store')

	}catch(error){
		//console.log(body);
		console.log(id);
		console.log("Error => " + error);
	}

	await next()
}