import axios from "axios"

export async function listAllSettlements(ctx: Context, next: () => Promise<any>){
	const {
		vtex: {
			route: {
				params: { giftcardId, transactionId },
			},
		},
	} = ctx

	const http = axios.create({
	    headers: {
	      VtexIdclientAutCookie: ctx.vtex.authToken,
	      "Cache-Control"   : "no-cache",
	      "X-Vtex-Use-Https": true,
	      "Authorization": "Bearer 33e4528f253db5d911a690856f0b77cfa98103c8231bffff869f179125d17d28e52bfadb9f205",
	    }
	});

	try{
		const url      = `https://servaxo1.vicom.mx/tafqaGC/API/v1/system/backoffice/getTid?store=${ctx.vtex.account}&tid=${transactionId}`
		const { data } = await http.get(url);

		const today         = new Date();
		const resposeStatus = data.operationId;
		let response        = {};

		if(resposeStatus == "success"){
			ctx.status = 200;
			response = [{
				oid : `${transactionId}`,
				value: 0,
				//value: balance,
				date : `${today.toISOString()}`
			}];
		}else{
			ctx.status = 401;
		}

		console.log(" == List Settlement Cencellations == ")
		console.log(giftcardId)
		console.log(transactionId)
		console.log(response)

		ctx.body = response;
		ctx.set('Cache-Control', 'no-cache,no-store');


	}catch(error){
		console.log(error);
	}

	await next()
}
