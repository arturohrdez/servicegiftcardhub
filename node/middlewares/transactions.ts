import { json } from 'co-body'
import axios from "axios"

export async function transactions(ctx: Context, next: () => Promise<any>) {
  const body = await json(ctx.req)

  const {
    vtex: {
      route: { params }
    }
  } = ctx

  const { id } = params


  const http = axios.create({
    headers: {
      VtexIdclientAutCookie: ctx.vtex.authToken,
      "Cache-Control"   : "no-cache",
      "X-Vtex-Use-Https": true,
      "Authorization": "Bearer 33e4528f253db5d911a690856f0b77cfa98103c8231bffff869f179125d17d28e52bfadb9f205",
    }
  });

  try{
    const url = `https://servaxo1.vicom.mx/tafqaGC/API/v1/system/backoffice/getBalance?token=${id}&store=${ctx.vtex.account}`;
    //console.log(url);
    
    const { data } = await http.get(url)

    ctx.state.transactionData = data
    ctx.state.transactionData.transaction = body

    /*console.log("== TRANSACTION ==");
    console.log(ctx.state.transactionData);
    console.log(ctx.state.transactionData.transaction);*/
  }catch(error){
    console.log("Error => " + error);
  }

  await next()
}
